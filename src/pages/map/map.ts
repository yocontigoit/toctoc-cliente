import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  Platform
} from "ionic-angular";
import { Observable } from "rxjs/Observable";
import { ServicesProvider } from "../../providers/services/services";
import { TranslateService } from '@ngx-translate/core';

declare var google: any;

@IonicPage()
@Component({
  selector: "page-map",
  templateUrl: "map.html"
})
export class MapPage {
  @ViewChild("map") mapElement: ElementRef;
  @ViewChild("searchbar", { read: ElementRef }) searchbar: ElementRef;
  addressElement: HTMLInputElement = null;

  map: any;
  lat: any;
  lng: any;
  lati: any;
  lngi: any;
  servicioID: any;
  dire: any;
  apto: any = '';
  buscador: any = '';
  DC: string;
  FLAT: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public zone: NgZone,
    public platform: Platform,
    public translateService: TranslateService,
    public _providerService: ServicesProvider
  ) {
    this.platform.ready().then(() => this.loadMaps());
    this.lat = this.navParams.get("lat");
    this.lng = this.navParams.get("lng");
    this.servicioID = this.navParams.get("servicioID");
    this.DC = translateService.instant('SUMMARY.AD');
    this.FLAT = translateService.instant('MAPPAGE.FLAT');
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MapPage");
  }

  goBack() {
    this.navCtrl.pop();
  }

  loadMaps() {
    if (!!google) {
      this.initializeMap();
      this.initAutocomplete();
    } else {
      this.errorAlert(
        "Error",
        "Something went wrong with the Internet Connection. Please check your Internet."
      );
    }
  }

  errorAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "OK",
          handler: data => {
            this.loadMaps();
          }
        }
      ]
    });
    alert.present();
  }

  initAutocomplete(): void {
    // reference : https://github.com/driftyco/ionic/issues/7223
    this.addressElement = this.searchbar.nativeElement.querySelector(
      ".searchbar-input"
    );
    this.createAutocomplete(this.addressElement).subscribe(location => {
      console.log("Searchdata", location);

      let options = {
        center: location,
        zoom: 18
      };
      this.map.setOptions(options);
      this.addMarker(location, "Mein gesuchter Standort");
    });
  }

  initializeMap() {
    this.zone.run(() => {
      const location = { lat: this.lat, lng: this.lng };
      var mapEle = this.mapElement.nativeElement;
      this.map = new google.maps.Map(mapEle, {
        center: location,
        zoom: 18,
        streetViewControl: false,
        fullscreenControl: false,
        mapTypeControl: false
      });

      this.addMarker(location, "Mein gesuchter Standort");
    });
  }

  createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
    const autocomplete = new google.maps.places.Autocomplete(addressEl);
    console.log('autoC:', autocomplete);
    
    autocomplete.bindTo("bounds", this.map);
    return new Observable((sub: any) => {
      google.maps.event.addListener(autocomplete, "place_changed", () => {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          sub.error({
            message: "Autocomplete returned place with no geometry"
          });
        } else {
          console.log("Search Lat", place.geometry.location.lat());
          console.log("Search Lng", place.geometry.location.lng());
          console.log("Place: ", place);
          console.log("Format Add:", place.formatted_address);
          
          this.lati = place.geometry.location.lat();
          this.lngi = place.geometry.location.lng();
          this.dire = place.formatted_address;
          sub.next(place.geometry.location);
        }
      });
    });
  }

  addMarker(position, content) {
    console.log(position);
    new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: position
    });
  }

  saveDire(){
    console.log('direccion', this.dire);
    if (this.dire != undefined) {
      this._providerService.updateLocationEntrega(
        this.lati,
        this.lngi,
        this.servicioID,
        this.dire,
        this.apto
      ); 
    }else{
      this._providerService._updateLocationEntrega(
        this.servicioID,
        this.buscador,
        this.apto
      ); 
    }
  }

  goBackListo() {
    this.navCtrl.pop();
  }
}
